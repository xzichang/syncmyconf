PS1=$(printf "\n%s@%s:%s\n%s" "\e[30m\e[48;5;45m\\u" "\\H" "\e[48;5;40m\\w\e[49m\e[39m" "\e]2;\\w\007\\!\\$" )

alias ll='colorls -Glh '
alias lvim='nvim-qt --nvim /home/gaozc/.local/bin/lvim'
alias ee='nvim-qt'


export http_proxy=http://192.168.2.1:8001
export https_proxy=$http_proxy
export all_proxy=$http_proxy

export PATH=/home/gaozc/.local/bin:$PATH



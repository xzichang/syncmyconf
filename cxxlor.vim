set background=dark

hi clear
let g:colors_name = 'cxxlor'

let s:t_Co = &t_Co

if (has('termguicolors') && &termguicolors) || has('gui_running')
  let g:terminal_ansi_colors = ['#7f7f8c', '#cd5c5c', '#9acd32', '#bdb76b', '#75a0ff', '#eeee00', '#cd853f', '#666666', '#8a7f7f', '#ff0000', '#89fb98', '#f0e68c', '#6dceeb', '#ffde9b', '#ffa0a0', '#c2bfa5']
  " Nvim uses g:terminal_color_{0-15} instead
  for i in range(g:terminal_ansi_colors->len())
    let g:terminal_color_{i} = g:terminal_ansi_colors[i]
  endfor
endif


hi! link Terminal Normal
hi! link LineNrAbove LineNr
hi! link LineNrBelow LineNr
hi! link CurSearch Search
hi! link CursorLineFold CursorLine
hi! link CursorLineSign CursorLine
hi! link EndOfBuffer NonText
hi! link MessageWindow Pmenu
hi! link PopupNotification Todo

" Color names
" Black=#000000
" DimGray=#696969
" Gray=#bebebe
" White=#ffffff


hi Normal guifg=#ffffff guibg=#000000 gui=NONE cterm=NONE
hi LineNr guifg=#696969 guibg=NONE gui=NONE cterm=NONE
hi StatusLine guifg=#ffffff guibg=#696969 gui=bold cterm=NONE
hi StatusLineNC guifg=#bebebe guibg=#696969 gui=NONE cterm=NONE
hi StatusLineTerm guifg=#ffffff guibg=#000000 gui=bold cterm=NONE
hi StatusLineTermNC guifg=#bebebe guibg=#000000 gui=NONE cterm=NONE
hi VertSplit guifg=#bebebe guibg=#696969 gui=NONE cterm=NONE
hi NonText guifg=#696969 guibg=#000000 gui=NONE cterm=NONE

hi Pmenu guifg=#ffffff guibg=#666666 gui=NONE cterm=NONE
hi PmenuSel guifg=#333333 guibg=#f0e68c gui=NONE cterm=NONE
hi PmenuSbar guifg=NONE guibg=#333333 gui=NONE cterm=NONE
hi PmenuThumb guifg=NONE guibg=#c2bfa5 gui=NONE cterm=NONE

hi TabLine guifg=#333333 guibg=#c2bfa5 gui=NONE cterm=NONE
hi TabLineFill guifg=NONE guibg=#c2bfa5 gui=NONE cterm=NONE
hi TabLineSel guifg=#333333 guibg=#f0e68c gui=NONE cterm=NONE
hi ToolbarLine guifg=NONE guibg=#666666 gui=NONE cterm=NONE
hi ToolbarButton guifg=#333333 guibg=#ffde9b gui=bold cterm=bold
hi SpecialKey guifg=#9acd32 guibg=NONE gui=NONE cterm=NONE
hi Folded guifg=#eeee00 guibg=#4d4d4d gui=NONE cterm=NONE
hi Visual guifg=#f0e68c guibg=#6b8e24 gui=NONE cterm=NONE
hi VisualNOS guifg=#f0e68c guibg=#6dceeb gui=NONE cterm=NONE
hi FoldColumn guifg=#eeee00 guibg=#4d4d4d gui=NONE cterm=NONE
hi CursorLine guifg=NONE guibg=#666666 gui=NONE cterm=NONE
hi CursorColumn guifg=NONE guibg=#666666 gui=NONE cterm=NONE
hi CursorLineNr guifg=#eeee00 guibg=NONE gui=bold cterm=bold
hi QuickFixLine guifg=#333333 guibg=#f0e68c gui=NONE cterm=NONE
hi SignColumn guifg=NONE guibg=NONE gui=NONE ctermfg=NONE ctermbg=NONE cterm=NONE
hi Underlined guifg=#75a0ff guibg=NONE gui=underline cterm=underline
hi Error guifg=#ff0000 guibg=#ffffff gui=reverse cterm=reverse
hi ErrorMsg guifg=#ff0000 guibg=#ffffff gui=reverse cterm=reverse
hi ModeMsg guifg=#ffde9b guibg=NONE gui=bold cterm=bold
hi WarningMsg guifg=#cd5c5c guibg=NONE gui=bold cterm=bold
hi MoreMsg guifg=#9acd32 guibg=NONE gui=bold cterm=bold
hi Question guifg=#89fb98 guibg=NONE gui=bold cterm=bold
hi Todo guifg=#ff0000 guibg=#eeee00 gui=NONE cterm=NONE
hi MatchParen guifg=#7f7f8c guibg=#bdb76b gui=NONE cterm=NONE
hi Search guifg=#f0e68c guibg=#7f7f8c gui=NONE cterm=NONE
hi IncSearch guifg=#f0e68c guibg=#cd853f gui=NONE cterm=NONE
hi WildMenu guifg=#333333 guibg=#eeee00 gui=NONE cterm=NONE
hi ColorColumn guifg=#ffffff guibg=#cd5c5c gui=NONE cterm=NONE
hi Cursor guifg=#333333 guibg=#f0e68c gui=NONE cterm=NONE
hi lCursor guifg=#333333 guibg=#ff0000 gui=NONE cterm=NONE
hi debugPC guifg=#666666 guibg=NONE gui=reverse cterm=reverse
hi debugBreakpoint guifg=#ffa0a0 guibg=NONE gui=reverse cterm=reverse
hi SpellBad guifg=#cd5c5c guibg=NONE guisp=#cd5c5c gui=undercurl cterm=underline
hi SpellCap guifg=#75a0ff guibg=NONE guisp=#75a0ff gui=undercurl cterm=underline
hi SpellLocal guifg=#ffde9b guibg=NONE guisp=#ffde9b gui=undercurl cterm=underline
hi SpellRare guifg=#9acd32 guibg=NONE guisp=#9acd32 gui=undercurl cterm=underline


" Olive=#808000
" LimeGreen=#32cd32 Comment
" DeepPink=#ff1493 PreProc
" Olive=#808000 Constant
" White=#ffffff Statement
" Gold=#ffd700 Type
"
" WebGreen=#008000 Identifier
" LightGreen=#90ee90 ClassMethodStatic
" Chocolate=#d2691e Field 
" Bisque=#ffe4c4 Parameter
" Orange=#ffa500 

hi Comment guifg=#32cd32 guibg=NONE gui=NONE cterm=NONE
hi PreProc guifg=#ff1493 guibg=NONE gui=NONE cterm=NONE
hi Constant guifg=#808000 guibg=NONE gui=NONE cterm=NONE
hi Statement guifg=#ffffff guibg=NONE gui=bold cterm=bold
hi Type guifg=#ffd700 guibg=NONE gui=NONE cterm=NONE
hi Identifier guifg=#008000 guibg=NONE gui=NONE cterm=NONE
hi LspCxxHlSymMethod guifg=#008000
hi LspCxxHlSymField guifg=#d2691e
hi LspCxxHlsymParameter guifg=#ffe4c4
hi LspCxxHlsymVariable guifg=#ffa500



hi Directory guifg=#808000 guibg=NONE gui=NONE cterm=NONE
hi Special guifg=#00ffff guibg=NONE gui=bold cterm=bold
hi Conceal guifg=#666666 guibg=NONE gui=NONE cterm=NONE
hi Ignore guifg=NONE guibg=NONE gui=NONE ctermfg=NONE ctermbg=NONE cterm=NONE
hi Title guifg=#cd5c5c guibg=NONE gui=bold cterm=bold
hi DiffAdd guifg=#ffffff guibg=#5f875f gui=NONE cterm=NONE
hi DiffChange guifg=#ffffff guibg=#5f87af gui=NONE cterm=NONE
hi DiffText guifg=#000000 guibg=#c6c6c6 gui=NONE cterm=NONE
hi DiffDelete guifg=#ffffff guibg=#af5faf gui=NONE cterm=NONE

" vim: et ts=2 sw=2

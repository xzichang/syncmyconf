set number
set tabstop=2 
set shiftwidth=2
set expandtab
set smartindent
set showcmd
set wrap

call plug#begin()

Plug 'preservim/nerdtree'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'jackguo380/vim-lsp-cxx-highlight'
Plug 'm-pilia/vim-ccls'
Plug 'tikhomirov/vim-glsl'

call plug#end()

colorscheme cxxlor

nnoremap <F4>t :NERDTreeToggle<CR> 
nnoremap <F4>r :NERDTreeRefreshRoot<CR>

if executable('ccls')
	au User lsp_setup call lsp#register_server({
		\ 'name': 'ccls',
		\ 'cmd': {server_info->['ccls']},
		\ 'root_uri': {server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'compile_commands.json'))},
		\ 'initialization_options': {
		\   'highlight': {'lsRanges' : v:true},
		\	'cache': {'directory': '/tmp/cache/ccls'}
		\ },
		\ 'whitelist': ['c', 'cpp', 'objc', 'objcpp', 'ipp'],
		\ })
endif


function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> [g <plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)

    noremap gD :LspDocumentDiagnostics<CR>

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')

    let g:asyncomplete_auto_popup = 1
    imap <expr> <c-,> pumvisible() ? asyncomplete#close_popup() : asyncomplete#force_refresh()
    let g:lsp_cxx_hl_use_text_props = 1

    autocmd BufEnter *.c,*.cpp,*.hpp call lsp_cxx_hl#hl#enable()

    nnoremap <F5>f :!clang-format -i %<CR>
    
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

"let g:lsp_log_verbose = 1
"let g:lsp_log_file = expand('~/vim-lsp.log')
"

